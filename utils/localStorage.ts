type Task = {
    id: string,
    text: string,
    checked: boolean
}

class LocalStorage {
    getAllTasks(): Task[] | [] {
        const tasksLocalStorage = localStorage.getItem("tasks")
        if (tasksLocalStorage !== null) {
            return JSON.parse(tasksLocalStorage)
        }
        return []
    }

    getActiveTasks(): Task[] | [] {
        const activeTask: Task[] = this.getAllTasks().filter(task => task.checked === false)
        return activeTask
    }

    getCompletedTasks(): Task[] | [] {
        const completedTask: Task[] = this.getAllTasks().filter(task => task.checked !== false)
        return completedTask
    }

    removeTasks(id: string): void {
        const result: Task[] = this.getAllTasks().filter(task => task.id !== id)

        localStorage.setItem("tasks", JSON.stringify(result))
        this.getCompletedTasks()
    }

    addTask(text: string): void {
        const tasks: Task[] = this.getAllTasks()

        if (text !== "") tasks.push({ id: new Date().toISOString(), text, checked: false })
        localStorage.setItem("tasks", JSON.stringify(tasks))
    }

    checked(id: string): void {
        const tasks: Task[] = this.getAllTasks()

        tasks.forEach(task => {
            if (task.id === id) task.checked = !task.checked
        });

        localStorage.setItem("tasks", JSON.stringify(tasks))
    }

    removeCompletedTasks(): void {
        const activeTask: Task[] = this.getAllTasks().filter(task => task.checked === false)

        localStorage.setItem("tasks", JSON.stringify(activeTask))
    }
}


const createLocalStorage = new LocalStorage()