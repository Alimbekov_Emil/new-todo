var LocalStorage = /** @class */ (function () {
    function LocalStorage() {
    }
    LocalStorage.prototype.getAllTasks = function () {
        var tasksLocalStorage = localStorage.getItem("tasks");
        if (tasksLocalStorage !== null) {
            return JSON.parse(tasksLocalStorage);
        }
        return [];
    };
    LocalStorage.prototype.getActiveTasks = function () {
        var activeTask = this.getAllTasks().filter(function (task) { return task.checked === false; });
        return activeTask;
    };
    LocalStorage.prototype.getCompletedTasks = function () {
        var completedTask = this.getAllTasks().filter(function (task) { return task.checked !== false; });
        return completedTask;
    };
    LocalStorage.prototype.removeTasks = function (id) {
        var result = this.getAllTasks().filter(function (task) { return task.id !== id; });
        localStorage.setItem("tasks", JSON.stringify(result));
        this.getCompletedTasks();
    };
    LocalStorage.prototype.addTask = function (text) {
        var tasks = this.getAllTasks();
        if (text !== "")
            tasks.push({ id: new Date().toISOString(), text: text, checked: false });
        localStorage.setItem("tasks", JSON.stringify(tasks));
    };
    LocalStorage.prototype.checked = function (id) {
        var tasks = this.getAllTasks();
        tasks.forEach(function (task) {
            if (task.id === id)
                task.checked = !task.checked;
        });
        localStorage.setItem("tasks", JSON.stringify(tasks));
    };
    LocalStorage.prototype.removeCompletedTasks = function () {
        var activeTask = this.getAllTasks().filter(function (task) { return task.checked === false; });
        localStorage.setItem("tasks", JSON.stringify(activeTask));
    };
    return LocalStorage;
}());
var createLocalStorage = new LocalStorage();
