class Tasks {
    removeTaskToLocalStorage(id: string): void {
        createLocalStorage.removeTasks(id)
        counter.renderCounter()
        buttonClearCompleted.renderButtonClear()
        this.render()
    }

    addTaskLocalStorage(): void {
        const input: any = document.querySelector("#field")
        createLocalStorage.addTask(input.value)
        counter.renderCounter()
        buttonClearCompleted.renderButtonClear()
        this.render()
    }

    checkedTaskLocalStorage(id: string): void {
        createLocalStorage.checked(id)
        counter.renderCounter()
        buttonClearCompleted.renderButtonClear()
    }

    render(array = createLocalStorage.getAllTasks()): void {
        let htmlTasks = `
            <label class="todo_item">
                <input type="text"id="field" placeholder="What needs to be done?"/>
                <button class="btn" onclick=tasks.addTaskLocalStorage()>Add</button> 
            </label>`
        array.forEach((task): void => {
            htmlTasks +=
                `<label class="todo_item">
                <input type="checkbox" class="checkbox" ${task.checked ? "checked" : ""}  onclick=tasks.checkedTaskLocalStorage('${task.id}')  />
                <span>${task.text}</span>
                <button id="btn_delete" onclick="tasks.removeTaskToLocalStorage('${task.id}')">X</button>
             </label>`
        })

        ROOT_TASKS.innerHTML = htmlTasks
    }
}


const tasks = new Tasks()
tasks.render()