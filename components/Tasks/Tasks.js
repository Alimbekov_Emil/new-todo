var Tasks = /** @class */ (function () {
    function Tasks() {
    }
    Tasks.prototype.removeTaskToLocalStorage = function (id) {
        createLocalStorage.removeTasks(id);
        counter.renderCounter();
        buttonClearCompleted.renderButtonClear();
        this.render();
    };
    Tasks.prototype.addTaskLocalStorage = function () {
        var input = document.querySelector("#field");
        createLocalStorage.addTask(input.value);
        counter.renderCounter();
        buttonClearCompleted.renderButtonClear();
        this.render();
    };
    Tasks.prototype.checkedTaskLocalStorage = function (id) {
        createLocalStorage.checked(id);
        counter.renderCounter();
        buttonClearCompleted.renderButtonClear();
    };
    Tasks.prototype.render = function (array) {
        if (array === void 0) { array = createLocalStorage.getAllTasks(); }
        var htmlTasks = "\n            <label class=\"todo_item\">\n                <input type=\"text\"id=\"field\" placeholder=\"What needs to be done?\"/>\n                <button class=\"btn\" onclick=tasks.addTaskLocalStorage()>Add</button> \n            </label>";
        array.forEach(function (task) {
            htmlTasks +=
                "<label class=\"todo_item\">\n                <input type=\"checkbox\" class=\"checkbox\" " + (task.checked ? "checked" : "") + "  onclick=tasks.checkedTaskLocalStorage('" + task.id + "')  />\n                <span>" + task.text + "</span>\n                <button id=\"btn_delete\" onclick=\"tasks.removeTaskToLocalStorage('" + task.id + "')\">X</button>\n             </label>";
        });
        ROOT_TASKS.innerHTML = htmlTasks;
    };
    return Tasks;
}());
var tasks = new Tasks();
tasks.render();
