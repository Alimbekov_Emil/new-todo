var Navigation = /** @class */ (function () {
    function Navigation() {
    }
    Navigation.prototype.all = function () {
        tasks.render();
    };
    Navigation.prototype.active = function () {
        tasks.render(createLocalStorage.getActiveTasks());
    };
    Navigation.prototype.computed = function () {
        tasks.render(createLocalStorage.getCompletedTasks());
    };
    Navigation.prototype.render = function () {
        var html = "\n            <button id=\"navigation_btn\" onclick=\"navigation.all()\">All</button>\n            <button id=\"navigation_btn\" onclick=\"navigation.active()\">Active</button>\n            <button id=\"navigation_btn\" onclick=\"navigation.computed()\">Completed</button>\n        ";
        ROOT_NAV.innerHTML = html;
    };
    return Navigation;
}());
var navigation = new Navigation();
navigation.render();
