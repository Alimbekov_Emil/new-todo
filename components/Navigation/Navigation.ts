class Navigation {
    all(): void {
        tasks.render()
    }

    active(): void {
        tasks.render(createLocalStorage.getActiveTasks())
    }

    computed(): void {
        tasks.render(createLocalStorage.getCompletedTasks())
    }

    render() {
        const html = `
            <button id="navigation_btn" onclick="navigation.all()">All</button>
            <button id="navigation_btn" onclick="navigation.active()">Active</button>
            <button id="navigation_btn" onclick="navigation.computed()">Completed</button>
        `
        ROOT_NAV.innerHTML = html
    }
}


const navigation = new Navigation()
navigation.render()