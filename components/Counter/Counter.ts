class Counter {

    renderCounter(): void {
        this.render(createLocalStorage.getAllTasks().filter(task => task.checked === false).length)
    }

    render(activeTask): void {
        const html = `
        <div class="counter">
               ${activeTask} items left
        </div>
        `
        ROOT_COUNTER.innerHTML = html
    }
}


const counter = new Counter()

counter.renderCounter()