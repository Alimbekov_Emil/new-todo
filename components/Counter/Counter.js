var Counter = /** @class */ (function () {
    function Counter() {
    }
    Counter.prototype.renderCounter = function () {
        this.render(createLocalStorage.getAllTasks().filter(function (task) { return task.checked === false; }).length);
    };
    Counter.prototype.render = function (activeTask) {
        var html = "\n        <div class=\"counter\">\n               " + activeTask + " items left\n        </div>\n        ";
        ROOT_COUNTER.innerHTML = html;
    };
    return Counter;
}());
var counter = new Counter();
counter.renderCounter();
