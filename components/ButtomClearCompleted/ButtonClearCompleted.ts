class ButtonClearCompleted {
    renderButtonClear(): void {
        const activeTaskCount: Task[] = createLocalStorage.getAllTasks().filter(task => task.checked !== false)
        this.render(activeTaskCount.length)
    }

    removeCompletedTask(): void {
        createLocalStorage.removeCompletedTasks()
        tasks.render()
        this.renderButtonClear()
    }

    render(count): void {
        const html = `
            <button onclick="buttonClearCompleted.removeCompletedTask()">Clear Completed (${count})</button>
        `
        ROOT_BUTTON_CLEAR.innerHTML = html
    }
}

const buttonClearCompleted = new ButtonClearCompleted()
buttonClearCompleted.renderButtonClear()