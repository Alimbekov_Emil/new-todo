var ButtonClearCompleted = /** @class */ (function () {
    function ButtonClearCompleted() {
    }
    ButtonClearCompleted.prototype.renderButtonClear = function () {
        var activeTaskCount = createLocalStorage.getAllTasks().filter(function (task) { return task.checked !== false; });
        this.render(activeTaskCount.length);
    };
    ButtonClearCompleted.prototype.removeCompletedTask = function () {
        createLocalStorage.removeCompletedTasks();
        tasks.render();
        this.renderButtonClear();
    };
    ButtonClearCompleted.prototype.render = function (count) {
        var html = "\n            <button onclick=\"buttonClearCompleted.removeCompletedTask()\">Clear Completed (" + count + ")</button>\n        ";
        ROOT_BUTTON_CLEAR.innerHTML = html;
    };
    return ButtonClearCompleted;
}());
var buttonClearCompleted = new ButtonClearCompleted();
buttonClearCompleted.renderButtonClear();
